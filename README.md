# RPN Calculator

The RPN Calculator implementation is written in Kotlin (1.3.20) as per [airwallex-oo-test.pdf](docs/airwallex-oo-test.pdf) requirements.

## Dependencies

Java 1.8+ and internet access to download kotlin dependencies via Gradle build.

## Quick Start

```
git clone https://bitbucket.org/andrewbruno/rpn-calculator.git
cd rpn-calculator/
./gradlew build && java -jar build/libs/rpncalculator.jar 
```

## Build & Test

`./gradlew clean test`

## Extra features

 * `q` exits the program gracefully
 * Extra spaces are allowed between operands and operators

## 
