package rpncalculator.calculator

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import rpncalculator.view.OutputHandler

class CalculatorTest {

    class ConsoleMock : OutputHandler() {
        override fun prompt() {
            lastDisplayMessage = "$ "
        }

        var lastDisplayMessage = ""
        override fun display(message: String) {
            lastDisplayMessage = "$lastDisplayMessage $message".trim()
        }

        fun getLastDisplayMessageAndClear(): String {
            val message = lastDisplayMessage
            lastDisplayMessage = ""
            return message
        }
    }

    private val console = ConsoleMock()
    private val calculator = Calculator(console)

    @AfterEach
    fun clear() {
        console.lastDisplayMessage = ""
    }

    @Test
    fun `verify do nothing when blank`() {
        calculator.execute("")
        assertEquals("stack:", console.getLastDisplayMessageAndClear())
    }

    @Test
    fun `verify example 1`() {
        calculator.execute("5 2")
        assertEquals("stack: 5 2", console.getLastDisplayMessageAndClear())
    }

    @Test
    fun `verify example 2`() {
        // The example says that 2 sqrt = stack: 1.4142135623 however using
        // a computer calculator set to 10 decimal places, I get 1.4142135624
        // Using Rounding ROUND_HALF_UP which is the most common taught in schools,
        // the rounding of 15 decimal places to 10 goes from:
        // 1.414213562373095 -> 1.4142135624 because of the 7 after the 3
        // However, since example 2 says 1.4142135623 I have changed the rounding to ROUND_DOWN
        // in order to pass this test as per examples.  See config.Precision
        //
        // Also, with this rounding, the accuracy is not great
        // For example 2 sqrt 2 sqrt * return stack: 1.9999999999
        calculator.execute("2 sqrt")
        assertEquals("stack: 1.4142135623", console.getLastDisplayMessageAndClear())

        calculator.execute("clear 9 sqrt")
        assertEquals("stack: 3", console.getLastDisplayMessageAndClear())
    }

    @Test
    fun `verify example 3`() {
        calculator.execute("5 2 -")
        assertEquals("stack: 3", console.getLastDisplayMessageAndClear())

        calculator.execute("3 -")
        assertEquals("stack: 0", console.getLastDisplayMessageAndClear())

        calculator.execute("clear")
        assertEquals("stack:", console.getLastDisplayMessageAndClear())
    }

    @Test
    fun `verify example 4`() {
        calculator.execute("5 4 3 2")
        assertEquals("stack: 5 4 3 2", console.getLastDisplayMessageAndClear())

        calculator.execute("undo undo *")
        assertEquals("stack: 20", console.getLastDisplayMessageAndClear())

        calculator.execute("5 *")
        assertEquals("stack: 100", console.getLastDisplayMessageAndClear())

        calculator.execute("undo")
        assertEquals("stack: 20 5", console.getLastDisplayMessageAndClear())
    }

    @Test
    fun `verify example 5`() {
        calculator.execute("7 12 2 /")
        assertEquals("stack: 7 6", console.getLastDisplayMessageAndClear())

        calculator.execute("*")
        assertEquals("stack: 42", console.getLastDisplayMessageAndClear())

        calculator.execute("4 /")
        assertEquals("stack: 10.5", console.getLastDisplayMessageAndClear())
    }

    @Test
    fun `verify example 6`() {
        calculator.execute("1 2 3 4 5")
        assertEquals("stack: 1 2 3 4 5", console.getLastDisplayMessageAndClear())

        calculator.execute("*")
        assertEquals("stack: 1 2 3 20", console.getLastDisplayMessageAndClear())

        calculator.execute("clear 3 4 -")
        assertEquals("stack: -1", console.getLastDisplayMessageAndClear())
    }

    @Test
    fun `verify example 7`() {
        calculator.execute("1 2 3 4 5")
        assertEquals("stack: 1 2 3 4 5", console.getLastDisplayMessageAndClear())

        calculator.execute("* * * *")
        assertEquals("stack: 120", console.getLastDisplayMessageAndClear())
    }

    @Test
    fun `verify example 8`() {
        // The documentation provided says position 15 is invalid, however it should be 8
        // for this example
        calculator.execute("1 2 3 * 5 + * * 6 5")
        assertEquals("command * (position: 8): insufficient parameters "
                + "stack: 11 "
                + "(the 6 and 5 were not pushed on to the stack due to the previous error)",
                console.getLastDisplayMessageAndClear())
    }

    @Test
    fun `verify sqrt 15 and back`() {
        calculator.execute("15 sqrt 15 sqrt *")
        assertEquals("stack: 15", console.getLastDisplayMessageAndClear())
    }
}
