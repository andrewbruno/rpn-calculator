package rpncalculator.view

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.mockito.Mockito

class ConsoleTest {

    class ConsoleMock : OutputHandler() {
        override fun prompt() {
            lastDisplayMessage = "> "
        }

        var lastDisplayMessage = ""
        override fun display(message: String) {
            lastDisplayMessage = message
        }
    }

    private val console = ConsoleMock()

    @Test
    fun `test interface implements methods`() {
        val expected = "Foo Bar"
        console.display(expected)
        assertEquals(expected, console.lastDisplayMessage)
    }

    @Test
    fun `test interface implements prompt`() {
        assertNotNull(console)
        console.prompt()
        assertEquals("> ", console.lastDisplayMessage)
    }

    @Test
    fun `test console actually fires display`() {
        val c = Mockito.mock(Console::class.java)
        c.display("Foo")
        Mockito.verify(c).display("Foo")
    }

    @Test
    fun `console lastDisplay should always be blank when blank entered`() {
        Assertions.assertEquals("", console.lastDisplayMessage)
    }
}

