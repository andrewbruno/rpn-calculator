package rpncalculator.command

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import rpncalculator.config.Precision
import rpncalculator.exception.InvalidOperatorException
import rpncalculator.util.ArrayBigDecimalDeque
import java.math.BigDecimal
import java.util.*

class CommandTest {
    private val position: Int = 3
    private val values = ArrayBigDecimalDeque()
    private val history = ArrayDeque<ArrayBigDecimalDeque>()

    @BeforeEach
    fun setUp() {
        values.add(BigDecimal(5))
        values.add(BigDecimal(9))
    }

    @Test
    fun `verify command plus`() {
        Command.execute("+", values, history, position)
        assertEquals("14", values.first().toPlainString())
    }

    @Test
    fun `verify command minus`() {
        Command.execute("-", values, history, position)
        assertEquals("-4", values.first().toPlainString())
    }

    @Test
    fun `verify command multiply`() {
        Command.execute("*", values, history, position)
        assertEquals("45", values.first().toPlainString())
    }

    @Test
    fun `verify command division`() {
        Command.execute("/", values, history, position)
        assertEquals("0.555555555555555", values.first().toPlainString())
    }

    @Test
    fun `verify command sqrt`() {
        Command.execute("sqrt", values, history, position)
        assertEquals(Precision.storageScale, values.elementAt(1).scale())
        assertEquals("3", values.elementAt(1).stripTrailingZeros().toPlainString())
    }

    @Test
    fun `verify command clear`() {
        Command.execute("clear", values, history, position)
        assertEquals(0, values.size)
    }

    @Test
    fun `verify invalid command stack mixed with values`() {
        var error = ""
        try {
            Command.execute("stack", values, history, position)
        } catch (e: InvalidOperatorException) {
            error = e.message!!
        }

        assertEquals("command stack (position: 3): invalid", error)
    }

    @Test
    fun `verify invalid command`() {
        var error = ""
        try {
            Command.execute("foo", values, history, position)
        } catch (e: InvalidOperatorException) {
            error = e.message!!
        }

        assertEquals("command foo (position: 3): invalid", error)
    }

}
