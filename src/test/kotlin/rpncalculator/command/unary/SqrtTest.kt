package rpncalculator.command.unary

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import rpncalculator.config.Precision
import rpncalculator.exception.InsufficientParametersException
import rpncalculator.util.ArrayBigDecimalDeque
import java.math.BigDecimal
import java.util.*

class SqrtTest {
    private val operator: String = "sqrt"
    private val position: Int = 2
    private var values = ArrayBigDecimalDeque()
    private val history = ArrayDeque<ArrayBigDecimalDeque>()

    @BeforeEach
    fun setUp() {
        values.clear()
    }

    @Test
    fun `verify sqrt 2`() {
        values.add(BigDecimal(2).setScale(Precision.storageScale, Precision.roundingMode))
        Sqrt.action(operator, position, values, history)
        val expected = BigDecimal(1.4142135623730951).setScale(Precision.storageScale, Precision.roundingMode)
        val calculated = values.first()
        assertEquals(expected, calculated)
    }

    @Test
    fun `verify sqrt 9`() {
        values.add(BigDecimal(9))
        Sqrt.action(operator, position, values, history)
        val expected = BigDecimal(3).setScale(Precision.storageScale, Precision.roundingMode)
        val calculated = values.first()
        assertEquals(expected, calculated)
    }

    @Test
    fun `verify insufficient parameters`() {
        var error = ""
        try {
            Sqrt.action(operator, position, values, history)
        } catch (e: InsufficientParametersException) {
            error = e.message!!
        }
        assertEquals("command sqrt (position: 2): insufficient parameters", error)
    }
}
