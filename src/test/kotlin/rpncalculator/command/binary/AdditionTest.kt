package rpncalculator.command.binary

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import rpncalculator.exception.InsufficientParametersException
import rpncalculator.util.ArrayBigDecimalDeque
import java.math.BigDecimal
import java.util.*

class AdditionTest {
    private val operator: String = "+"
    private val position: Int = 3
    private val values = ArrayBigDecimalDeque()
    private val history = ArrayDeque<ArrayBigDecimalDeque>()

    @BeforeEach
    fun setUp() {
        values.add(BigDecimal(1))
        values.add(BigDecimal(2))
    }

    @Test
    fun `verify addition`() {
        Addition.action(operator, position, values, history)
        assertEquals(BigDecimal(3), values.first())
    }

    @Test
    fun `verify insufficient parameters`() {
        values.removeLast()

        var error = ""
        try {
            Addition.action(operator, position, values, history)
        } catch (e: InsufficientParametersException) {
            error = e.message!!
        }
        assertEquals("command + (position: 3): insufficient parameters", error)
    }
}
