package rpncalculator.command.binary

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import rpncalculator.config.Precision
import rpncalculator.exception.InsufficientParametersException
import rpncalculator.util.ArrayBigDecimalDeque
import java.math.BigDecimal
import java.util.*

class DivisionTest {
    private val operator: String = "/"
    private val position: Int = 3
    private val values = ArrayBigDecimalDeque()
    private val history = ArrayDeque<ArrayBigDecimalDeque>()

    @BeforeEach
    fun setUp() {
        values.add(BigDecimal(1))
        values.add(BigDecimal(6))
    }

    @Test
    fun `verify division`() {
        Division.action(operator, position, values, history)
        assertEquals(BigDecimal(0.166666666666667)
                .setScale(Precision.storageScale, Precision.roundingMode),
                values.first())
    }

    @Test
    fun `verify insufficient parameters`() {
        values.removeLast()

        var error = ""
        try {
            Division.action(operator, position, values, history)
        } catch (e: InsufficientParametersException) {
            error = e.message!!
        }
        assertEquals("command / (position: 3): insufficient parameters", error)
    }
}
