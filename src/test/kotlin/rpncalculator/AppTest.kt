package rpncalculator

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import rpncalculator.view.OutputHandler

class AppTest {

    class ConsoleMock : OutputHandler() {
        override fun prompt() {
            lastDisplayMessage = "> "
        }

        var lastDisplayMessage = ""
        override fun display(message: String) {
            lastDisplayMessage = message
        }
    }

    private val console = ConsoleMock()

    @Test
    fun `test app accepts output handler`() {
        val app = App(console)
        assertNotNull(app)
    }

    @Test
    fun `test app gets input`() {
        val appMock = mock(App::class.java)
        `when`(appMock.getInput()).thenReturn("q")
        assertEquals(appMock.getInput(), "q")
    }
}
