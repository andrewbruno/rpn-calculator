package rpncalculator.exception

class InvalidOperatorException(operator: String, position: Int) :
        CalculatorException("command $operator (position: $position): invalid")
