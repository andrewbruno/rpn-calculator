package rpncalculator.exception

open class CalculatorException(message: String) : Exception(message)
