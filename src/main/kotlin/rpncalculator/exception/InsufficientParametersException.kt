package rpncalculator.exception

class InsufficientParametersException(operator: String, position: Int) :
        CalculatorException("command $operator (position: $position): insufficient parameters")
