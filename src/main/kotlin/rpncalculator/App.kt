package rpncalculator

import rpncalculator.calculator.Calculator
import rpncalculator.view.Console
import rpncalculator.view.OutputHandler

const val QUIT = "q"

class App(
        private val output: OutputHandler
) {
    internal fun getInput(): String {
        return readLine()!!.trim()
    }

    fun start() {
        val calculator = Calculator(output)
        output.display("\n-- RPN Calculator -- \n")
        loop@ while (true) {
            output.prompt()
            val instruction = getInput()

            if (instruction == QUIT) break@loop

            calculator.execute(instruction)
        }
    }
}

fun main() {
    val output: OutputHandler = Console()
    App(output).start()
}
