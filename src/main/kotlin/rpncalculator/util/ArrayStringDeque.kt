package rpncalculator.util

import java.math.BigDecimal
import java.util.*

class ArrayBigDecimalDeque : ArrayDeque<BigDecimal>() {
    fun secondLastOperandOrNull(): BigDecimal? {
        if (this.size > 1) {
            return this.elementAt(this.size - 2)
        }
        return null
    }

    fun lastOperandOrNull(): BigDecimal? {
        if (this.size > 0) {
            return this.elementAt(this.size - 1)
        }
        return null
    }

    override fun clone(): ArrayBigDecimalDeque {
        return super.clone() as ArrayBigDecimalDeque
    }
}
