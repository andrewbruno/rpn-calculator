package rpncalculator.command.binary

import rpncalculator.config.Precision
import rpncalculator.exception.InsufficientParametersException
import rpncalculator.util.ArrayBigDecimalDeque
import java.util.*

class Division : BinaryOperator() {
    companion object {
        fun action(operator: String,
                   position: Int,
                   values: ArrayBigDecimalDeque,
                   history: ArrayDeque<ArrayBigDecimalDeque>) {

            val operands = getOperands(values) ?: throw InsufficientParametersException(operator, position)

            val answer = operands.first.divide(operands.second, Precision.storageScale, Precision.roundingMode)

            updateAnswer(values, operands, answer)
            updateHistory(history, operands)
        }
    }
}
