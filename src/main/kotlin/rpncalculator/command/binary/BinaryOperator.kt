package rpncalculator.command.binary

import rpncalculator.util.ArrayBigDecimalDeque
import java.math.BigDecimal
import java.util.*

/**
 * The number of operands of an operator is called its arity.
 * Based on arity, this operator is classified as binary (2 operands)
 */
abstract class BinaryOperator {

    companion object {
        fun getOperands(values: ArrayBigDecimalDeque): Pair<BigDecimal, BigDecimal>? {
            val operand2 = values.lastOperandOrNull()
            val operand1 = values.secondLastOperandOrNull()

            if (operand1 == null || operand2 == null) {
                return null
            }
            return Pair(operand1, operand2)
        }

        fun updateAnswer(values: ArrayBigDecimalDeque, operands: Pair<BigDecimal, BigDecimal>, answer: BigDecimal) {
            values.remove(operands.first)
            values.remove(operands.second)
            values.add(answer)
        }

        fun updateHistory(history: ArrayDeque<ArrayBigDecimalDeque>, operands: Pair<BigDecimal, BigDecimal>) {
            val rollBack = ArrayBigDecimalDeque()
            rollBack.add(operands.first)
            rollBack.add(operands.second)
            history.push(rollBack)
        }
    }
}

