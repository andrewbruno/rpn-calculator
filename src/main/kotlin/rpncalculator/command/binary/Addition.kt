package rpncalculator.command.binary

import rpncalculator.exception.InsufficientParametersException
import rpncalculator.util.ArrayBigDecimalDeque
import java.util.*

class Addition : BinaryOperator() {
    companion object {
        fun action(operator: String,
                   position: Int,
                   values: ArrayBigDecimalDeque,
                   history: ArrayDeque<ArrayBigDecimalDeque>) {

            val operands = getOperands(values) ?: throw InsufficientParametersException(operator, position)

            val answer = operands.first.plus(operands.second)

            updateAnswer(values, operands, answer)
            updateHistory(history, operands)
        }
    }
}
