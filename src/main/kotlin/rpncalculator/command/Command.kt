package rpncalculator.command

import rpncalculator.command.binary.Addition
import rpncalculator.command.binary.Division
import rpncalculator.command.binary.Multiplication
import rpncalculator.command.binary.Subtraction
import rpncalculator.command.nullary.Clear
import rpncalculator.command.nullary.Stack
import rpncalculator.command.nullary.Undo
import rpncalculator.command.unary.Sqrt
import rpncalculator.exception.InvalidOperatorException
import rpncalculator.util.ArrayBigDecimalDeque
import java.util.*

enum class Command(
        val perform: (
                command: String,
                position: Int,
                values: ArrayBigDecimalDeque,
                history: ArrayDeque<ArrayBigDecimalDeque>
        ) -> Unit
) {
    PLUS({ c, p, v, h -> Addition.action(c, p, v, h) }),
    MINUS({ c, p, v, h -> Subtraction.action(c, p, v, h) }),
    MULTIPLY({ c, p, v, h -> Multiplication.action(c, p, v, h) }),
    DIVIDE({ c, p, v, h -> Division.action(c, p, v, h) }),
    SQRT({ c, p, v, h -> Sqrt.action(c, p, v, h) }),
    CLEAR({ _, _, v, h -> Clear(v, h) }),
    UNDO({ _, _, v, h -> Undo(v, h) }),
    STACK({ c, p, _, _ -> Stack(c, p) });

    companion object {
        private val cmdLookUp = hashMapOf(
                "+" to PLUS,
                "-" to MINUS,
                "*" to MULTIPLY,
                "/" to DIVIDE,
                "sqrt" to SQRT,
                "clear" to CLEAR,
                "undo" to UNDO,
                "stack" to STACK)

        fun execute(command: String,
                    values: ArrayBigDecimalDeque,
                    history: ArrayDeque<ArrayBigDecimalDeque>,
                    position: Int
        ) {
            val operator = cmdLookUp[command] ?: throw InvalidOperatorException(command, position)
            operator.perform(command, position, values, history)
        }
    }
}
