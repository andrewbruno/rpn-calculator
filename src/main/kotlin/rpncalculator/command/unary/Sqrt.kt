package rpncalculator.command.unary

import rpncalculator.config.Precision
import rpncalculator.exception.InsufficientParametersException
import rpncalculator.util.ArrayBigDecimalDeque
import java.math.BigDecimal
import java.util.*

class Sqrt : UnaryOperator() {
    companion object {
        fun action(operator: String,
                   position: Int,
                   values: ArrayBigDecimalDeque,
                   history: ArrayDeque<ArrayBigDecimalDeque>) {

            val operand = getOperand(values) ?: throw InsufficientParametersException(operator, position)
            updateAnswer(values, operand, sqrt(operand))
            updateHistory(history, operand)
        }
    }
}

val sqrt = { n: BigDecimal ->
    BigDecimal(Math.sqrt(n.toDouble())).setScale(
            Precision.storageScale, Precision.roundingMode)
}
