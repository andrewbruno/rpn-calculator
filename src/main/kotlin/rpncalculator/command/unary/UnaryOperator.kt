package rpncalculator.command.unary

import rpncalculator.util.ArrayBigDecimalDeque
import java.math.BigDecimal
import java.util.*

/**
 * The number of operands of an operator is called its arity.
 * Based on arity, this operator is classified as unary (1 operand)
 */
abstract class UnaryOperator {
    companion object {
        fun getOperand(values: ArrayBigDecimalDeque): BigDecimal? {
            return values.lastOperandOrNull()
        }

        fun updateAnswer(values: ArrayBigDecimalDeque, operand: BigDecimal, answer: BigDecimal) {
            values.remove(operand)
            values.add(answer)
        }

        fun updateHistory(history: ArrayDeque<ArrayBigDecimalDeque>, operand: BigDecimal) {
            val rollBack = ArrayBigDecimalDeque()
            rollBack.add(operand)
            history.push(rollBack)
        }
    }
}

