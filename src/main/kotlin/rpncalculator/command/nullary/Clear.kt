package rpncalculator.command.nullary

import rpncalculator.util.ArrayBigDecimalDeque
import java.util.*

class Clear(values: ArrayBigDecimalDeque, history: ArrayDeque<ArrayBigDecimalDeque>) {
    init {
        history.clear()
        values.clear()
    }
}
