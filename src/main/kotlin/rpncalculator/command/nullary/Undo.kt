package rpncalculator.command.nullary

import rpncalculator.util.ArrayBigDecimalDeque
import java.util.*

class Undo(values: ArrayBigDecimalDeque, history: ArrayDeque<ArrayBigDecimalDeque>) {
    init {
        if (values.isNotEmpty()) values.removeLast()
        if (!history.isNullOrEmpty()) {
            history.pop().forEach { oldValue -> values.add(oldValue) }
        }
    }
}
