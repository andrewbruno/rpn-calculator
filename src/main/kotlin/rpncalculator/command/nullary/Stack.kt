package rpncalculator.command.nullary

import rpncalculator.exception.InvalidOperatorException

class Stack(command: String, position: Int) {
    init {
        if (position != 1)
            throw InvalidOperatorException(command, position)
    }
}
