package rpncalculator.calculator

import rpncalculator.command.Command
import rpncalculator.config.Precision
import rpncalculator.exception.CalculatorException
import rpncalculator.util.ArrayBigDecimalDeque
import rpncalculator.view.OutputHandler
import java.util.*

class Calculator(private val output: OutputHandler) {

    private val values = ArrayBigDecimalDeque()
    private var history = ArrayDeque<ArrayBigDecimalDeque>()

    fun execute(rawCommand: String) {

        val args = rawCommand.split(" ").map { it }.filter { it != "" }

        for ((index, value) in args.withIndex()) {
            val item = value.toBigDecimalOrNull()?.setScale(Precision.storageScale, Precision.roundingMode)
            if (item != null) {
                values.add(item)
            } else {
                try {
                    Command.execute(value, values, history, index + 1)
                } catch (e: CalculatorException) {
                    output.display(e.message!!)
                    output.display(values)
                    val remainingValues = args.subList(index + 1, args.size)
                    if (!remainingValues.isEmpty()) output.display(remainingValues)
                    return // stop processing stack
                }
            }
        }

        output.display(values)
    }
}

