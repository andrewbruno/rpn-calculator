package rpncalculator.view

class Console : OutputHandler() {
    override fun prompt() {
        print("")
    }

    override fun display(message: String) {
        if (!message.isBlank()) println(message)
    }
}
