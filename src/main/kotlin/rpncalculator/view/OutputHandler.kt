package rpncalculator.view

import rpncalculator.config.Precision
import rpncalculator.util.ArrayBigDecimalDeque

abstract class OutputHandler {
    abstract fun display(message: String)
    abstract fun prompt()

    private fun formatMessage(stack: ArrayBigDecimalDeque): String {
        var output = ""
        stack.forEach {
            val number = it.setScale(Precision.displayScale, Precision.roundingMode).stripTrailingZeros()
            output = "$output ${number.toPlainString()}"
        }
        return "stack: ${output.trim()}"
    }

    private fun formatMessage(args: List<String>): String {
        var output = "(the"
        for ((index, it) in args.withIndex()) {
            output = when {
                args.size == 1 -> "$output $it was"
                index == args.size - 1 -> "$output and $it were"
                index == args.size - 2 -> "$output $it"
                else -> "$output $it,"
            }
        }
        return "$output not pushed on to the stack due to the previous error)"
    }

    fun display(stack: ArrayBigDecimalDeque) {
        display(formatMessage(stack))
    }

    fun display(args: List<String>) {
        display(formatMessage(args))
    }
}
