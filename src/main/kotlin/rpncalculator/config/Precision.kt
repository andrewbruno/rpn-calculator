package rpncalculator.config

import java.math.BigDecimal

class Precision {
    companion object {
        // Set to ROUND_DOWN instead of standard ROUND_HALF_UP in order to pass Example 2
        // i.e. 2 sqrt -> 1.4142135623
        // See notes in CalculatorTest
        const val roundingMode = BigDecimal.ROUND_DOWN

        const val displayScale: Int = 10
        const val storageScale: Int = 15
    }
}



